#include <stdio.h>
#include <stdlib.h>

/**
 * @brief Prints an error message in the standard output stream
 */
void print_err() {
    printf("[error]");
}

/**
 * @brief Frees the allocated memory
 * @param arr Integer array
 */
void free_arr(int *arr) {
    if (arr) free(arr);
}

/**
 * @brief Scans a size value
 * @param s Pointer to already allocated object
 * @return 0 if no errors
 */
int scan_size(size_t *s) {
    return scanf("%zu", s) != 1;
}

/**
 * @brief Scans an integer array from the standard input stream
 * @param n Length of the array
 * @param arr Pointer to the array (or NULL if an error)
 * @return 0 if no errors
 */
int scan_arr(size_t *n, int **arr) {
    if (scan_size(n)) return 1;
    *arr = NULL;
    if (*n > 0) {
        *arr = (int *)malloc(*n * sizeof(int));
        if (*arr) {
            for (unsigned i = 0; i < (*n); ++i) {
                if (scanf("%d", *arr + i) != 1) {
                    free(*arr);

                    return 1;
                }
            }
        }
    }

    return 0;
}

/**
 * @brief Prints the array in the standard output stream
 * @param arr Integer array
 * @param n Length of the array
 */
void print_arr(const int *arr, const size_t n) {
    if (arr) {
        for (unsigned i = 0; i < (n - 1); ++i) {
            printf("%d ", arr[i]);
        }
        printf("%d", arr[n - 1]);
    }
}

/**
 * @brief Swaps the values of two pointers
 * @param a One value's pointer
 * @param b Another value's pointer
 */
void swap(int *a, int *b) {
    int tmp = *a;
    *a = (*b);
    *b = tmp;
}

/**
 * @brief Subdivides the range into smaller and equal or larger elements
 * relative to the pivot
 * @param arr Integer array
 * @param li Low index
 * @param ri High index
 */
unsigned partition(int *arr, const size_t li, const size_t ri) {
    int pivot = arr[(li + ri) / 2];
    int l = li;
    int r = ri;
    while (l <= r) {
        while (arr[l] < pivot) ++l;
        while (arr[r] > pivot) --r;
        if (l <= r) swap(arr + (l++), arr + (r--));
    }
    return l;
}

/**
 * @brief Sorts the range using the quick sort algorithm
 * @param arr Integer array
 * @param li Low index
 * @param ri High index
 */
void quick_sort(int *arr, const size_t li, const size_t ri) {
    if (li < ri) {
        unsigned p = partition(arr, li, ri);
        quick_sort(arr, li, p - 1);
        quick_sort(arr, p, ri);
    }
}

/**
 * @brief Returns a new array that contains M max elements
 * @param arr Integer array
 * @param n Length of the array
 * @param res New array containing M max elements (NULL if an error or m = 0)
 * @param m Length of a new array
 * @return 0 if no errors
 */
int max(int *arr, const size_t n, int **res, const size_t m) {
    if (m > n) return 1;
    *res = NULL;
    if (m > 0) {
        quick_sort(arr, 0, n - 1);
        *res = (int *)malloc(m * sizeof(int));
        if (*res) {
            for (unsigned i = 0, j = n - 1; i < m; ++i, --j) {
                (*res)[i] = arr[j];
            }
        }
    }

    return 0;
}

int main()
{
    size_t n, m;
    int *arr = NULL;
    if (scan_arr(&n, &arr) || scan_size(&m)) {
        print_err();

        return 0;
    }
    int *res = NULL;
    if (max(arr, n, &res, m)) {
        print_err();
        free_arr(arr);

        return 0;
    }
    print_arr(res, m);
    free_arr(arr);
    free_arr(res);

    return 0;
}
